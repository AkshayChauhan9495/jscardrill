module.exports = function problem5(inventory, problem4) {
  let year = problem4(inventory)
  let olderCars = [];
  for (let index = 0; index < year.length; index++) {
    if (year[index] < 2000) {
      olderCars.push(year[index]);
    }
  }
  console.log(olderCars.length)
  return olderCars;
}
