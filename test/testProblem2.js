let inventory = require('../inventory.js');
let problem2 = require('../problem2.js');
const result = problem2(inventory);
const expectedResult = "Last car is a Lincoln Town Car";
if (result === expectedResult) {
    console.log(result);
} else {
    console.log("Results are different.");
}