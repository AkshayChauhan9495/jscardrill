let inventory = require('../inventory.js');
let problem1 = require('../problem1.js');
const testId = 33;
const result = problem1(inventory, testId);
const expectedResult = "Car 33 is a 2011 Jeep Wrangler";
if (result === expectedResult) {
    console.log(result);
} else {
    console.log("Results are different.");
}
