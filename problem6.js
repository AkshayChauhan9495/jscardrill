module.exports = function problem6(inventory) {
  let bmwAudi = [];
  for (let index = 0; index < inventory.length; index++) {
    if (inventory[index].car_make === "Audi" || inventory[index].car_make === "BMW") {
      bmwAudi.push(inventory[index].car_model);
    }
  }
  return (JSON.stringify(bmwAudi));
}
