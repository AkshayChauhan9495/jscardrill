module.exports = function problem3(inventory) {
  let carModels = [];
  for (let index = 0; index < inventory.length; index++) {
    carModels.push(inventory[index].car_model);
  }
  for (let row = 0; row < carModels.length; row++) {
    for (let column = row + 1; column < carModels.length; column++) {
      if (carModels[row].toLowerCase() > carModels[column].toLowerCase()) {
        let temp = carModels[row];
        carModels[row] = carModels[column];
        carModels[column] = temp;
      };
    }
  }
  return carModels
  }
